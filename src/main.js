/**
 *
 * Llamada a los módulos de NodeJS y ElectronJS necesarios
 *
 */
/* Módulo ElectronJS de NodeJS */
const electron = require('electron')
/* Módulo app de ElectronJS para controlar el ciclo de vida de los eventos de la app*/
const app = electron.app
/* Módulo BrowserWindow de ElectronJS para crear ventanas nativas del navegador */
const BrowserWindow = electron.BrowserWindow

const Menu = electron.Menu
/* Módulo ipcMain de ElectronJS para la comuncicación entre ventanas */
const ipcMain = electron.ipcMain

/* Módulo path de NodeJS para trabajar con rutas de archivos y directorios */
const path = require('path')
/* Módulo url de NodeJS para tpara la resolución y análisis de URLs */
const url = require('url')


/**
 *
 * Referencia global a las ventanas de la aplicación
 *
 */
let mainWindow
let addDataWindow

/**
 *
 * Creación de la ventana Main
 *
 */
function create_mainWindow () {
	/* Instancia de BW para esta ventana */
	mainWindow = new BrowserWindow({
		/* Propiedades de esta ventana */
		title: 'Inicio'
	})

	/* Formateo de la ventana y asignación de una vista */
	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'views/main.html'),
		protocol: 'file',
		slashes: true
	}))

	const mainMenu = Menu.buildFromTemplate(main_templateMenu)
	Menu.setApplicationMenu(mainMenu);

	mainWindow.on('closed', () => {
		app.quit()
	})

	request_ipcMain()
}

/**
 *
 * Creación de la ventana Add Data
 *
 */
function create_addWindow () {
	/* Instancia de BW para esta ventana */
	addDataWindow = new BrowserWindow({
		/* Propiedades de esta ventana */
		title: 'Add Data',
		width: 500,
		height: 300
	})

	/* Formateo de la ventana y asignación de una vista */
	addDataWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'views/addData.html'),
		protocol: 'file',
		slashes: true
	}))
}

/* Arreglo con objetos que conforman el MENU de la app **/
const main_templateMenu = [
	{
		label: 'Data',
		accelerator: 'CmdOrCtrl+N',
		click() {
			create_addWindow();
		}
	},
	{
		label: 'DevTools',
		accelerator: 'CmdOrCtrl+Shift+I',
		click(item, focusedWindow){
			focusedWindow.toggleDevTools();
		}
	}, 
	{
		label: 'Quit',
		accelerator: 'CmdOrCtrl+Q',
		click(){
			app.quit();
		}
	}
];

// Agrega el nombre de la aplicacion al menu si es encuentra en el sistema MacOS
if(process.platform === 'darwin'){
	main_templateMenu.unshift({
		label: app.getName(),
		submenu: [
			{
				label: 'Copyright'
			},
			{
				label: 'About'
			}
		]
	})
}

// Funcion ipcMain que recive y envia informacion a los procesos renderer process
function request_ipcMain () {
	// body... 
	ipcMain.on('request-send-data-to-main-window', (event, arg) => {
        // Recibe en este canal los argumentos y los envia a la ventana de AddWindow
        mainWindow.webContents.send('action-add-data', arg);
        // Cerrar el form cuando se envien los datos
        addDataWindow.close()
		//console.log('action-add-data', arg)
    });
}

/* Cuando todo esta cargado que ejecute lo siguiente */
app.on('ready', () => {
	create_mainWindow()
	//create_addWindow()
})

// Quita todo cuando las ventanas se cierran
app.on('window-all-closed', () => {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
})